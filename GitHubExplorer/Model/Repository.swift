//
//  Repository.swift
//  GitHubExplorer
//
//  Created by Andrey Grinevich on 10/30/18.
//  Copyright © 2018 Andrey Grinevich. All rights reserved.

import UIKit

class Repository {
    static var repositoryItem = [RepositoryItem]()
}

struct RepositoryItem {
    var repositoryName: String
    var repositoryStars: Int
    var repositoryOwner: String
    var repositoryDescription: String
    var repositoryLanguage: String
    var repositoryID: Int
}
