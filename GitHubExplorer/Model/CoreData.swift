//
//  CoreData.swift
//  GitHubExplorer
//
//  Created by Andrey Grinevich on 10/31/18.
//  Copyright © 2018 Andrey Grinevich. All rights reserved.
//

import UIKit
import CoreData

class CoreData {
    
    func fetchFromCoreData() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "LocalRepository")
        do {
            var items: [NSManagedObject] = []
            items = try managedContext.fetch(fetchRequest)
            for index in 0..<items.count {
                let item = items[index]
                
                let newItem = RepositoryItem.init(repositoryName: item.value(forKeyPath: "repositoryName") as? String ?? "Repository name",
                                                  repositoryStars: item.value(forKeyPath: "repositoryStars") as? Int ?? 0,
                                                  repositoryOwner: item.value(forKeyPath: "repositoryOwner") as? String ?? "Repository owner",
                                                  repositoryDescription: item.value(forKeyPath: "repositoryDescription") as? String ?? "Repository description",
                                                  repositoryLanguage: item.value(forKeyPath: "repositoryLanguage") as? String ?? "Artist language",
                                                  repositoryID: item.value(forKeyPath: "repositoryID") as? Int ?? 0)
                
                Repository.repositoryItem.append(newItem)
            }
            NotificationCenter.default.post(Notification.init(name: Notification.Name(rawValue: "RepositoriesListDidReceived")))
        } catch let error as NSError {
            print("Save error - \(error), \(error.userInfo.description)")
        }
    }
    
    func clearCoreData() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "LocalRepository")
        do {
            var item = try managedContext.fetch(fetchRequest)
            
            for index in 0..<item.count {
                let objectToDelete = item[index]
                managedContext.delete(objectToDelete)
            }
            do {
                try managedContext.save()
            } catch {
                print(error)
            }
        } catch let error as NSError {
            print("Save error - \(error), \(error.userInfo.description)")
        }
    }
}
