//
//  Extension.swift
//  GitHubExplorer
//
//  Created by Andrey Grinevich on 10/30/18.
//  Copyright © 2018 Andrey Grinevich. All rights reserved.
//

import UIKit

extension String {
    var encodeUrl: String { return self.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)! }
    var decodeUrl: String { return self.removingPercentEncoding! }
}
