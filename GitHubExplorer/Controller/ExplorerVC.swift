//
//  ExplorerVC.swift
//  GitHubExplorer
//
//  Created by Andrey Grinevich on 10/30/18.
//  Copyright © 2018 Andrey Grinevich. All rights reserved.
//

import UIKit
import CoreData

class ExplorerVC: UIViewController {

    @IBOutlet weak var repositoriesTableView: UITableView!
    
    var searchedUsername = ""
    let searchController = UISearchController(searchResultsController: nil)
   
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(updateTableView(notification:)), name: NSNotification.Name(rawValue: "RepositoriesListDidReceived"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(networkStatusChanged(_:)), name: NSNotification.Name(rawValue: ReachabilityStatusChangedNotification), object: nil)
        
        setSearchBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Reachability().monitorReachabilityChanges()
    }
    
    @objc func updateTableView(notification: Notification) {
        DispatchQueue.main.async {
            self.repositoriesTableView.reloadData()
        }
    }
    
    @objc func networkStatusChanged(_ notification: Notification) {
        let userInfo = (notification as NSNotification).userInfo
        if userInfo!.description.range(of: "Online") != nil {
            searchController.searchBar.placeholder = "Enter correct username"
        } else {
            searchController.searchBar.placeholder = "Offline mode"
            Repository.repositoryItem.removeAll()
            CoreData().fetchFromCoreData()
        }
    }
    
    func setSearchBar() {
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
        repositoriesTableView.tableHeaderView = searchController.searchBar
    }
    
    @IBAction func clearMemoryButton(_ sender: UIBarButtonItem) {
        let alert = UIAlertController(title: "Warning", message: "Do you really want remove all items of offline repository list?", preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { (action) in
            CoreData().clearCoreData()
            Repository.repositoryItem.removeAll()
            self.repositoriesTableView.reloadData()
            alert.dismiss(animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in alert.dismiss(animated: true, completion: nil) }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func getDataFromAPI(username: String) {
        
        let jsonUrlString = "https://api.github.com/users/\(username.decodeUrl)/repos"
        
        guard let url = URL(string: jsonUrlString) else { return }
        URLSession.shared.dataTask(with: url) { (optionalData, response, jsonError) in
            guard let data = optionalData else { return }
            do {
                let jsonData = try JSONSerialization.jsonObject(with: data, options: [])
                guard let gitArray = jsonData as? NSArray else { return print("Not correct NSArray")}
                Repository.repositoryItem.removeAll()
                for object in gitArray {
                    guard let currentRepository = object as? NSDictionary else { return print("Not correct NSDictionary")}
                    guard let ownerDictionary = currentRepository.value(forKey: "owner") as? NSDictionary else { return print("Not correct owner dictionary")}
                    
                    let newItem = RepositoryItem.init(repositoryName: currentRepository.value(forKey: "name") as? String ?? "Unknown repository",
                                                      repositoryStars: currentRepository.value(forKey: "stargazers_count") as? Int ?? 0,
                                                      repositoryOwner: ownerDictionary.value(forKey: "login") as? String ?? "Unknown owner",
                                                      repositoryDescription: currentRepository.value(forKey: "description") as? String ?? "Absent",
                                                      repositoryLanguage: currentRepository.value(forKey: "language") as? String ?? " not selecred",
                                                      repositoryID: currentRepository.value(forKey: "id") as? Int ?? 0)
                    Repository.repositoryItem.append(newItem)
                }
                NotificationCenter.default.post(Notification.init(name: Notification.Name(rawValue: "RepositoriesListDidReceived")))
            } catch let jsonError {
                print("Error code: ", jsonError)
            }
        }.resume()
    }
}
