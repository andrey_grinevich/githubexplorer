//
//  ExplorerVC+Extention.swift
//  GitHubExplorer
//
//  Created by Andrey Grinevich on 10/31/18.
//  Copyright © 2018 Andrey Grinevich. All rights reserved.
//

import UIKit

extension ExplorerVC: UITableViewDataSource, UITableViewDelegate, UISearchResultsUpdating, UISearchBarDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Repository.repositoryItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let repositoryCell = Bundle.main.loadNibNamed("RepositoryTableViewCell", owner: self, options: nil)?.first as! RepositoryTableViewCell
        if Repository.repositoryItem.count != 0 {
            repositoryCell.repositoryNameLabel.text = Repository.repositoryItem[indexPath.row].repositoryName
            repositoryCell.repositoryStarCounterLabel.text = "⭐️ Star: " + Repository.repositoryItem[indexPath.row].repositoryStars.description
        } else {
            searchController.searchBar.placeholder = "Enter correct username"
        }
        return repositoryCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let detailVC = storyboard.instantiateViewController(withIdentifier: "DetailVC") as! DetailVC
        detailVC.repositoryName = Repository.repositoryItem[indexPath.row].repositoryName
        detailVC.repositoryIndex = indexPath.row
        self.navigationController?.pushViewController(detailVC, animated: true)
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        guard let username = searchController.searchBar.text else {
            searchedUsername = "."
            return
        }
        searchedUsername = username
        getDataFromAPI(username: searchedUsername)
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchController.searchBar.endEditing(true)
    }
}

