//
//  DetailVC.swift
//  GitHubExplorer
//
//  Created by Andrey Grinevich on 10/30/18.
//  Copyright © 2018 Andrey Grinevich. All rights reserved.
//

import UIKit
import CoreData

class DetailVC: UIViewController {
    
    @IBOutlet weak var ownerLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var starsLabel: UILabel!
    @IBOutlet weak var languageLabel: UILabel!
    @IBOutlet weak var storedLabel: UILabel!

    var repositoryName = "Details"
    var repositoryIndex = 0
    var alreadyInMemory = false
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        title = repositoryName
        ownerLabel.text = "👨🏼‍🎓  Owner: " + Repository.repositoryItem[repositoryIndex].repositoryOwner
        descriptionLabel.text = Repository.repositoryItem[repositoryIndex].repositoryDescription
        starsLabel.text = "⭐️  Star: " + Repository.repositoryItem[repositoryIndex].repositoryStars.description
        languageLabel.text = "💻  Language: " + Repository.repositoryItem[repositoryIndex].repositoryLanguage
        
        storedLabel.alpha = 0
        checkRepositoryInMemory()
    }
    
    func checkRepositoryInMemory() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "LocalRepository")
        do {
            var items: [NSManagedObject] = []
            items = try managedContext.fetch(fetchRequest)
            for index in 0..<items.count {
                let item = items[index]
                let currentRepositoryID = item.value(forKeyPath: "repositoryID") as? Int ?? 0
                if currentRepositoryID == Repository.repositoryItem[repositoryIndex].repositoryID {
                    alreadyInMemory = true
                    storedLabel.alpha = 1
                }
            }
        } catch let error as NSError {
            print("Save error - \(error), \(error.userInfo.description)")
        }
    }
    
    @IBAction func saveToCoreDataButton(_ sender: UIButton) {
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContext = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "LocalRepository", in: managedContext)!
        let item = NSManagedObject(entity: entity, insertInto: managedContext)
        
        item.setValue(Repository.repositoryItem[repositoryIndex].repositoryName, forKey: "repositoryName")
        item.setValue(Repository.repositoryItem[repositoryIndex].repositoryStars, forKey: "repositoryStars")
        item.setValue(Repository.repositoryItem[repositoryIndex].repositoryOwner, forKey: "repositoryOwner")
        item.setValue(Repository.repositoryItem[repositoryIndex].repositoryDescription, forKey: "repositoryDescription")
        item.setValue(Repository.repositoryItem[repositoryIndex].repositoryLanguage, forKey: "repositoryLanguage")
        item.setValue(Repository.repositoryItem[repositoryIndex].repositoryID, forKey: "repositoryID")
        
        if !alreadyInMemory {
            do {
                try managedContext.save()
                storedLabel.alpha = 1
            } catch let error as NSError {
                print("Save error - \(error), \(error.userInfo.description)")
            }
        }
    }
}
