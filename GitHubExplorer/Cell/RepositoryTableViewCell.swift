//
//  RepositoryTableViewCell.swift
//  GitHubExplorer
//
//  Created by Andrey Grinevich on 10/30/18.
//  Copyright © 2018 Andrey Grinevich. All rights reserved.
//

import UIKit

class RepositoryTableViewCell: UITableViewCell {

    @IBOutlet weak var repositoryNameLabel: UILabel!
    @IBOutlet weak var repositoryStarCounterLabel: UILabel!
    
}
